---
layout: page
title: "Fête de la Science au LRI : contacts"
---

## Organisateurs

Laboratoire de recherche en informatique : Nicolas Thiéry <br />
<fetedelascience@lri.fr> – 06 77 90 32 79

Service Communication de l'U-PSUD : Anne-Karine Nicolas <br />
<communication.sciences@u-psud.fr> – 06 77 27 56 33

Service Communication du CNRS : Frédérique Trouslard <br />
<frederique.trouslard@dr4.cnrs.fr> – 06 61 81 83 93



## Liens utiles

[La Fête de la Science (site national)](https://www.fetedelascience.fr/)

[La Fête de la Science à l'université
Paris-Sud](http://www.sciences.u-psud.fr/fr/actualites/fete-de-la-science-edition-2016.html)

[Le LRI (Laboratoire de recherche en informatique)](https://www.lri.fr/index.php)

[Université Paris-Sud](http://www.u-psud.fr/fr/index.html)


