---
layout: page
title: Toutes les activités 2016 et 2017
---

<table border="10">
  <tr>
  {% for activite in site.activites %}
      {% include activite-in-table.html activite=activite year=site.year %}
  {% endfor %}
  </tr>
</table>
