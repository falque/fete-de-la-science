---
layout: page
title: "Fête de la Science au LRI : renseignements pratiques"
---


## Où aller

### Vendredi 12 octobre 2018 (journée des scolaires) :

Bâtiments 650 (Ada Lovelace) et 660 (Claude Shannon)<br>
Plateau du Moulon

<center>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=2.1026802062988286%2C48.67985408653669%2C2.235202789306641%2C48.74498365675653&amp;layer=mapnik&amp;marker=48.71242941020935%2C2.1689414978027344" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/?mlat=48.7124&amp;mlon=2.1689#map=14/48.7124/2.1689">Agrandissement de la carte</a></small>
</center>

Accès :
- En voiture : A6A, A6B ou A10 ; ou N118<br>
  Coordonnées GPS : N +48.712429 E +2.169195<br>
  Attention: en raison des travaux, l'accès se fait par la rue Francis Perrin<br>
- RER B (arrêt Orsay-Ville) puis bus 7 (arrêt Moulon)
- RER B (arrêt Le Guichet) puis bus 9 (arrêt IUT-Pôle d'Ingénierie)
- RER B (arrêt Massy-Palaiseau) puis bus 91-06B ou 91-06C ou 91-10
  (arrêt IUT-Pôle d'Ingénierie)

Les horaires de bus : <http://vianavigo.com/>

<a href="{{site.baseurl}}/public/batiment_lri.jpg"><img class="image"
src="{{site.baseurl}}/public/batiment_lri.jpg" alt="Le bâtiment 650 (Ada
Lovelace - LRI)"></a>



### Dimanche 14 octobre 2018 (journée tous publics) :

<a name="grand_public"></a>

Bâtiment 450, Campus d'Orsay (vallée)<br>
450 rue Claude Bernard, Orsay

<center>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=2.139973640441895%2C48.687305780529854%2C2.2212553024291997%2C48.718545554488635&amp;layer=mapnik&amp;marker=48.7029280913022%2C2.180614471435547" style="border: 1px solid black"></iframe><br/><small>
<a href="http://www.openstreetmap.org/?mlat=48.7029&amp;mlon=2.1806#map=15/48.7029/2.1806">Agrandissement de la carte</a></small>
</center>

Accès:
- <a href="http://www.openstreetmap.org/directions?engine=mapzen_foot&route=48.69732%2C2.18181%3B48.70320%2C2.18066#map=17/48.70026/2.17986">Depuis le RER B, arrêt Orsay-Ville</a> (10 minutes à pied)

## <a href="{{site.baseurl}}/contact/">Contact</a>


