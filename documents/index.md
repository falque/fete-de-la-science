---
layout: page
title: Documents
---

{% assign documents = site.static_files | where: "document", true | where: "extname", ".pdf" %}
<ul>
{% for document in documents %}
<li><a href="{{ document.path }}">{{ document.name }}</a></li>
{% endfor %}
</ul>

## Documents à imprimer


- plaquette et planning scolaire, class-code: un exemplaire par accompagnant;
- 123codez: un exemplaire par accompagnant primaire et collège;
- plaquette grand public: 25 exemplaires?
- affiches: deux exemplaires;
- flèches: ?
