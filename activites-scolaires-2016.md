---
layout: page
title: Activités pour les scolaires 2016
---

Le vendredi 14 octobre 2016, nous avons reçu dans les locaux du LRI
six classes du CE2 à la seconde pour nos activités scolaires:

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite['2016'].scolaires %}
      {% include activite-in-table.html activite=activite year='2016'%}
    {% endif %}
  {% endfor %}
  </tr>
</table>

Télécharger le [planning](../documents/planning-scolaires-2016.pdf) ou la
[plaquette]({{site.baseurl}}/documents/plaquette-scolaires-2016.pdf)
