---
layout: page
title: Activités pour le grand public
---

{% for activite in site.activites %}
  {% if activite.url contains "jonglerie" %}
     {% assign jonglerie = activite %}
  {% endif %}
{% endfor %}

Voici les activités qui seront présentées au grand public le dimanche
15 octobre 2017 au [bâtiment 450](/pratique#grand_public), de 14h00 à 18h00.

**L'entrée est libre, sans inscription**, en fonction des places
disponibles. Les activités durent environ 30 minutes et sont répétées
plusieurs fois dans l'après-midi. Notre attraction phare, le
spectacle
<a href="{{ site.baseurl }}{{ jonglerie.url }}">«{{ site.baseurl }}{{ jonglerie.title }}»</a>
sera présenté à {{ jonglerie['2017'].grand_public.horaires }}.

Télécharger la [Plaquette]({{site.baseurl}}/documents/plaquette-public-2017.pdf)

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite[site.year].grand_public %}
      {% include activite-in-table.html activite=activite year=site.year %}
    {% endif %}
  {% endfor %}
  </tr>
</table>
