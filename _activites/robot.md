---
layout: activite
title: "Fabrique ton robot"
author: nthiery
level: primaire, collège
'2016':
  scolaires:
    salle: Salle 445 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 336
'2017':
  scolaires:
    salle: Salle 445 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 450
    participants: 5 * 25
bgcolor: PaleTurquoise
keywords: 
logo: logo-arduino.png
images: 
- robot_1.jpg
- robot_2.jpg
myid: robot
duree: 30min
---

Dans cet atelier interactif, nous montrons comment on peut construire
un petit robot à partir de briques simples : des composants
électroniques, des lego, du carton, trois bouts de ficelle et un
micro-ordinateur [Arduino](https://fr.wikipedia.org/wiki/Arduino).

Un premier pas pour aller au-delà de l'usage des gadgets électroniques
tous faits, en s'appropriant la technologie pour fabriquer ses propres
objets répondant à ses envies et besoins. Et puis voir au passage
quelques concepts fondamentaux de la science informatique : qu'est-ce
qu'un ordinateur, un programme, un robot, un logiciel ou du matériel
libre.

## Pour aller plus loin

- Arduino, Thymio, microbit, ... Quel [matériel](materiel/) choisir?

- Une ébauche de [tutoriel](tutoriel/) pour reproduire chez vous les
  différents montages présentés pendant l'atelier, jusqu'au robot
  suiveur de ligne, ou mieux votre propre projet!

- À venir: liens vers les clubs de robotique de la région
