---
layout: activite
title: "Réalité virtuelle, réalité augmentée"
author:
- keith
- david
level: collège, lycée
'2016':
  scolaires:
    salle: Salle 445 - Bâtiment 650 Ada Lovelace
bgcolor: '#ff99cc'
keywords: 
logo: logo-virtuel.jpg
images:
- realite-virtuelle.jpg
- realite-augmentee.jpg
---

Deux visites en une ! Dans la première, vous plongerez dans un *monde
virtuel* : une salle 3D vous immergera au cœur d'une installation
industrielle interactive. Dans la seconde, la *réalité augmentée* rendra
visible ce qui ne l'est pas, des pokémons à la maintenance des machines
industrielles complexes.
