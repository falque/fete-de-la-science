---
layout: activite
title: Interagir avec des "vrais" objets sur des écrans tactiles
author:
  - appert
  - morales
level: collège, lycée # à partir de la 4e
'2017':
  grand_public:
    salle: ??? - Bâtiment 450
    participants: 40-50
bgcolor: 'Plum'
keywords:
logo: logo-tokens.jpg
images: tokens.jpg
myid: tokens
---

Les interfaces tangibles permettent d'utiliser des objets physiques pour dialoguer avec le monde numérique. En alliant le tangible et le numérique, ces interfaces présentent l'avantage de manipuler un concept par sa représentation physique, tout en gardant la flexibilité offerte par le monde numérique. On peut par exemple imaginer manipuler des figurines Lego pour contrôler un jeu vidéo sur une tablette.

Les <i>TouchTokens</i> sont des objets complètement passifs qui peuvent être utilisés sur n'importe quelle surface tactile, comme celle d'une tablette ou d'un smartphone. Le principe général de TouchTokens consiste à placer des encoches sur des objets passifs afin de contraindre la façon dont les utilisateurs attrapent ces objets. Ainsi, lorsque les utilisateurs tiennent un objet tout en étant en contact avec l'écran, la surface tactile capture une configuration de positions des doigts qui est spécifique à cet objet.

Cet atelier présente le principe des TouchTokens et de les essayer, et, pour ceux qui veulent aller plus loin, de relever le défi de construire ses propres TouchTokens.
