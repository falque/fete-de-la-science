---
layout: activite
title: "Fractales: choux, Bretagne et flocons de neige"
author:
- guenais
- emme
level: primaire, collège, lycée
'2017':
  grand_public:
    salle: Bâtiment 450
bgcolor: Yellow
keywords: mathématiques
logo: logo-fractales.jpg
myid: fractales
---

Quel rapport y a-t-il entre la côte de Bretagne, un choux Romanesco et
un flocon de neige mathématique ? Dans cet atelier animé par le
Laboratoire de Mathématiques d'Orsay, nous étudierons les analogie
entre ces objets en observant la répétition des formes à plusieurs
échelles. Nous pourrons mesurer, comparer, imaginer et construire ces
formes complexes appelées formes fractales.

<!-- Logo: https://commons.wikimedia.org/wiki/File:Chou_Romanesco-entier.jpg
     Photo par Roger prat — Travail personnel CC-BY-SA 3.0 !-->
