---
layout: activite
title: "Conférence : Pourquoi mon ordinateur calcule faux ?"
author: boldo
level: collège, lycée
'2016':
  scolaires:
    salle: Salle 455 - Bâtiment 650 Ada Lovelace
bgcolor: Yellow
keywords: 
logo: arithmetique-logo.png
images: 
---

Nous confions à nos ordinateurs de nombreux calculs (météo, simulations
aéronautiques, jeux vidéos, tableurs...) et nous considérons
naturellement que l'ordinateur fournira une réponse juste. 

Malheureusement, la machine a ses limites que l'esprit humain n'a pas.
Elle utilise une arithmétique dite flottante qui a ses contraintes :
- D'une part chaque calcul est effectué avec un certain nombre de
  chiffres (souvent environ 15 chiffres décimaux) et donc chaque calcul
  peut créer une erreur, certes faible, mais qui peut s'accumuler avec
  les précédentes pour fournir un résultat complètement faux. 
- D'autre part, les valeurs que l'ordinateur appréhende ont des limites
  vers l'infiniment petit et l'infiniment grand. Hors de ces bornes,
  l'ordinateur produit des valeurs spéciales souvent inattendues. 

Cet exposé montrera que l'ordinateur n'est pas infaillible ou plutôt que
son utilisation est parfois abusive.
