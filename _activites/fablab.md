---
layout: activite
title: "Le FabLab"
level: collège, lycée
'2016':
  scolaires:
    salle: FabLab - Bâtiment 660 Claude Shannon
  author:
  - divozzo
  - nthiery
'2017':
  scolaires:
    salle: FabLab - Bâtiment 660 Claude Shannon
  author:
  - divozzo
  - manaud
  - nthiery
bgcolor: Lavender
keywords: 
logo: logo-fablab.png
images: vue-fablab.jpg
myid: fablab
---

Fabuleux laboratoire ou laboratoire de fabrication ?!  Venez découvrir
et faire fonctionner une imprimante 3D, une découpeuse laser, une
machine à coudre à commande numérique, une découpeuse vinyle...

## Pour en savoir plus 

- [Site officiel du Fablab d'Orsay](http://www.digiscope.fr/fr/platforms/fablab)
- [Une interview présentant le
  Fablab](http://www.3dnatives.com/fablab-pour-chercheurs-digiscope/)
