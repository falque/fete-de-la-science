---
layout: activite
title: "Quelle est cette plante ? Lisons son ADN..."
level: primaire, collège

'2017':
  author:
  - froidevaux
  - andrieu
  - chaput
  - chevalier
  scolaires:
    salle: 465 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 450
bgcolor: LightGreen
keywords: 
logo: logo-adn.png
images: 
myid: adn
---

Un agriculteur souhaite savoir si dans son champ s'est introduit du
maïs OGM, c'est-à-dire un maïs qui a été modifié pour résister aux
insectes ravageurs et aux herbicides par exemple. Problème : à l’œil
nu, impossible de faire la différence entre les maïs modifiés et non
modifiés ! Venez découvrir comment la biologie et l'informatique
s'allient pour lui venir en aide, grâce à lecture de l'ADN de la
plante. Mais lire l'ADN n'est pas tout à fait aussi simple que tourner
les pages d'un livre...
