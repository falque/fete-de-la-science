---
layout: page
title: "Tutoriel: «Les entrées: sentir le monde»"
---

## Mesure de tension

- Téléverser le programme `Exemples -> Basics -> AnalogReadSerial`
- Ouvrir le moniteur série `Outils -> Moniteur série`

  Vous voyez défiler des nombres à l'écran.

- Brancher le fil entre `A0` et `GND` (0 volts)

  Cela affiche 0.
- Rebrancher le fil entre `A0` et `5V` (5 volts)

  Cela affiche 1023.

  Pourquoi 1023? Parce que $1023=2^{10}-1$! Voir l'[atelier sur le
  binaire](../../binaire).
- Rebrancher le fil entre `A0` et `3V` (3 volts)

  Prédire la valeur qui sera affichée!

  Et hop, une petite règle de trois.

Nous venons de fabriquer un *voltmètre*! `analogRead` permet de
mesurer une tension entre la terre (0V) et un pin comme `A0`.

C'est la seule manière qu'ait arduino de «sentir» son environnement.
Mais c'est suffisant! Lorsque l'on voudra mesurer une autre grandeur
physique (une quantité de lumière, un poids, ...), on utilisera un
*capteur* (et éventuellement un circuit électronique) pour convertir
cette mesure en tension.

## Mesure de lumière



## Défis

### Détecteur de passage

Construire un détecteur de passage, avec une lampe en face d'un
capteur de lumière; si un obstacle passe entre les deux, déclencher
l'alarme. Dans un premier temps, l'alarme pourra être signalée en
allumant la led pendant 5 secondes (ou plus si l'obstacle reste).
Ensuite, vous pourrez utiliser une des alarmes que l'on a vu dans le
tutoriel précédent.

Applications potentielles:

- Alarme anti intrusion
- Sécurité de porte de garage (ne pas refermer la porte s'il y a un obstacle)
- Déclenchement d'un appareil photo au passage d'un animal
- ...
