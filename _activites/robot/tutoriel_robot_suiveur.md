## Matériel

Voici les composants que j'ai utilisé pour fabriquer ce robot. La
plupart d'entre eux -- y compris l'arduino, se trouvent dans tout bon
magasin d'électronique. Certains nécessitent de passer par un
revendeur spécialisé; il y en a plusieurs en France; en l'occurence je
suis passé par [Snootlab](snootlab.com); à titre purement indicatif
j'ai précisé la référence et le prix chez eux au moment de mon achat.

| Reference | Nom                                                                       | Prix unitaire | Quantité | Total   |
| --------- | ---------------------                                                     | ------------  | -------- | ------: |
| SPK-12757 | SparkFun RedBoard, compatible Arduino                                     | 23,40 €       |        1 | 23,40 € |
| ADA-01438 | Adafruit Motor/Stepper/Servo Shield pour Arduino v2 Kit - v2.0 CMS montés | 21,60 €       |        1 | 21,60 € |
| ADA-00858 | Petit moteur stepper avec réducteur 5V 512 pas                            | 6,00 €        |        2 | 12,00 € |
| KIT-00001 | Kit 2+2 connecteurs empilables Arduino                                    | 1,71 €        |        1 | 1,71 €  |
| KIT-00011 | Kit connecteurs empilables Arduino 1.0                                    | 1.91 €        |        1 | 1.91 €  |
| POW-00004 | Bloc support 4 piles AA pour Arduino                                      | 3,71 €        |        1 | 3,71 €  |
| KIT-01010 | Kit 10 cordons 15cm                                                       | 3,50 €        |        2 | 7,00 €  |
| SEN-00005 | Photoresistance 4mm                                                       | 2,00 €        |        2 | 4,00 €  |
| RES-00029 | Ruban de 10 résistances 1 KOhms                                           | 0,90 €        |        1 | 0,90 €  |
| OPT-00003 | 10 Led 3mm vertes diffuses x10                                            | 1,51 €        |        1 | 1,51 €  |
| OPT-00003 | 10 Led 3mm rouge diffuses x10                                             | 1,51 €        |        1 | 1,51 €  |
| KIT-01028 | Plaque d'essai                                                            | 3,50 €        |        1 | 3,50 €  |
| --------- | ---------------                                                           | -----------   |      --- | ---:     |
| Total     |                                                                           |               |          | 82,75 € |

Oups, c'est au final plus cher que ce que j'avais annoncé pendant les
ateliers.

À cela se rajoute la structure mécanique. En l'occurence du légo
technique, mais une structure en carton pourrait faire l'affaire
(projet à venir: un plan pour la découpeuse laser).

Les roues ont été découpées au laser dans un fond de cageot de
fraises; voici le [plan](wheel.eps).

Dans l'optique d'explorer d'autres montages, il peut être rapidement
rentable d'acheter l'un des nombreux kits de démarrage.

## Montage

- [ ] Souder les connecteurs empilables (KIT-000011) sur la plaque Adafruit
- [ ] Réordonner les câbles sur les prises des moteurs pas à pas
- [ ] Utiliser les connecteurs empilables supplémentaires pour faire
      des prises débranchables pour la plaque Adafruit
- [ ] ...


TODO: compléter et ajouter les photos
