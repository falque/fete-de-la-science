---
layout: activite
title: "Wilder : le mur d'écrans"
level: collège, lycée
'2016':
  author:
  - gladin
  - hellequin
  - kooli
  - thorpe
  scolaires:
    salle: Wilder, Bâtiment 660 Claude Shannon
'2017':
  author:
  - gladin
  scolaires:
    salle: Wilder, Bâtiment 660 Claude Shannon

bgcolor: PeachPuff
keywords: 
logo: logo-wilder.jpg
images: 
- wilder_1.png
- wilder_2.jpg
myid: wilder
---

Venez découvrir la plateforme WILDER, un mur d'image haute résoluton
conçu pour être utilisé de près comme de loin. Ce qui vous attend :
10 machines en cluster, 12 m2 d'affichage, 15 lignes de 5 écrans, une
résoluton totale de 14400×4800 pixels, une surface tactile capable de
suivre 32 points de contacts simultanés, 8 caméras infrarouges permetant
de repérer dans l'espace des marqueurs rétro-luminescents...

## Pour aller plus loin

- [Page officielle de WILDER](http://www.digiscope.fr/fr/platforms/wilder)

- [Inauguration de WILDER en juin 2015](https://www.inria.fr/centre/saclay/actualites/wilder-un-mur-d-ecrans-qui-fait-entrer-la-recherche-dans-une-nouvelle-dimension)

