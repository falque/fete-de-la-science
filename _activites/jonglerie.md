---
layout: activite
title: "Jonglerie musicale, automates et combinatoire"
author:
- hivert
- delavenere
level: collège, lycée
'2016':
  scolaires:
    salle: Salle des thèses (435) - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Amphi H6 - Bâtiment 336
'2017':
  scolaires:
    salle: Salle des thèses (435) - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Amphi G1 - Bâtiment 450
    horaires: 14h30, 15h45, 17h
    participants: 60+60+40
bgcolor: LightPink
keywords:
logo: logo-jonglerie.jpg
images: expose-jonglerie.jpg
myid: jonglerie
---

Un spectacle en musique, né de la rencontre d'un chercheur et d'un
jongleur (<a href="https://www.youtube.com/watch?v=PGvtXRYh1Q8&feature=youtu.be"
target="_blank">extraits vidéo</a>).

Un scientifique qui essaye de comprendre un problème commence souvent
par une étape de «modélisation»: on simplifie le réel, on ne garde que
les informations essentielles, et on crée ce que l’on appelle un
modèle. Un jongleur doit garder la trace de ses expérimentations
visuelles; quand il essaie de croiser cela avec une approche musicale,
cela devient très complexe: il aurait besoin d’écrire une «partition»
de jonglerie.

Quand le scientifique et le jongleur se rencontrent, pédagogie et
spectacle, informatique et jonglerie, combinatoire et musique se
rencontrent aussi, pour le bonheur des petits et des grands.
