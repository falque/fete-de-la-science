---
layout: activite
title: "Le binaire"
level: primaire

'2016':
  author: nguyen
  scolaires:
    salle: Salle 435 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 336
'2017':
  author: bvaliron
  scolaires:
    salle: Salle 455 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 450
bgcolor: DarkSeaGreen
keywords: 
logo: logo-binaire.jpg
images: 
myid: binaire
---

Aujourd'hui les ordinateurs sont plus que des calculateurs géants. Ils
peuvent servir de bibliothèque, nous aider à écrire, trouver des
informations, jouer de la musique et même lire des films. 

Alors, comment stockent-ils toutes ces informations ? Que vous le
croyiez ou non, l'ordinateur n'utilise que deux éléments : le zéro et le
un !

Dans cet atelier interactif, nous découvrirons quelle est la différence
entre les *données* et les *informations* : les données
sont la matière première, les nombres avec lesquels l'ordinateur
travaille. Un ordinateur convertit ces données en informations (mots,
nombres et images) que vous et moi pouvons comprendre. Nous apprendrons
l'écriture binaire des nombres, comment les ordinateurs affichent des
images, et d'autres choses !
