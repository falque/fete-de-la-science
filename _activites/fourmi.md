---
layout: activite
title: Aide-moi à sortir !
author:
- falque
level: primaire, collège
'2016':
  scolaires:
    salle: Bibliothèque - Bâtiment 650 Ada Lovelace
'2017':
  scolaires:
    salle: bibliothèque - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Salle ??? - Bâtiment 450
    participants: ~5 * 20
bgcolor: LightYellow
keywords: 
logo: fourmi-logo.png
images: fourmi_1.jpg
myid: fourmi
---

La fourmi est enfermée dans un labyrinthe aux milles embûches. Elle est
toute perdue, mais courageuse et très obéissante. 

Ensemble nous la guiderons pas à pas jusqu'à la sortie. Ce sera
l'occasion d'écrire nos premiers programmes : donner une suite d'ordres
simples, les répéter, les adapter à l'environnement.
