---
layout: page
title: Activités pour les scolaires (en construction)
---

Voici les activités qui seront présentées aux scolaires le vendredi 13
octobre 2017 (9h00-18h00). Les inscriptions des classes se font auprès
du [service communication de
l'université](http://www.sciences.u-psud.fr/fr/actualites/fete-de-la-science-edition-2017/visites-1.html)
à partir de la mi-juin. Nous sommes complet pour 2017.

Télécharger le [planning provisoire](../documents/planning-scolaires-2017.pdf) ou la
[plaquette]({{site.baseurl}}/documents/plaquette-scolaires-2017.pdf)

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite[site.year].scolaires %}
      {% include activite-in-table.html activite=activite year=site.year %}
    {% endif %}
  {% endfor %}
  </tr>
</table>
