---
layout: page
title: Activités pour le grand public 2016
---

Le dimanche 16 octobre 2016 de 13h00 à 17h30, nous avons accueilli
plus de 150 personnes au bâtiment 336 pour nos activités grand public:

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite['2016'].grand_public %}
      {% include activite-in-table.html activite=activite year='2016' %}
    {% endif %}
  {% endfor %}
  </tr>
</table>

[Télécharger la plaquette (PDF)]({{site.baseurl}}/documents/plaquette-public-2016.pdf)
