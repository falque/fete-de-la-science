---
layout: page
title: L'équipe de la Fête de la Science au LRI
---


<table width="100%" border="0" cellspacing="1em">

{% for keyvalue in site.data.people %}
<tr>
  {% assign person = keyvalue[1] %}
<td><a name="{{keyvalue[0]}}"></a>
    <b>{% if person.url %}<a href="{{person.url}}">{% endif %}
    {{person.name}}
    {% if person.url %}</a>{% endif %}</b>
<br />
    {{person.job}}{% if person.affiliation %}, {{person.affiliation}}{% endif %}
    {% if person.role %}<br>{{person.role}}{% endif %}
    {% for activite in site.activites %}
      {% capture authors %}{{activite.author | join:','}},{{activite['2016'].author | join:','}},{{activite['2017'].author | join:','}}{%endcapture%}
      {% assign authors = authors | split: "," | uniq %}
      {% for author in authors %}
        {% if keyvalue[0] == author %}
         <br><span style="background-color:{{activite.bgcolor}}"><a href="{{ site.baseurl }}{{ activite.url }}">{{ activite.title }}</a></span>
        {% endif %}
      {% endfor %}
    {% endfor %}

</td>
<td align="center">
    {% if person.picture %}<a href="{{site.baseurl}}/public/{{person.picture}}" alt="{{person.name}}"><img class="portrait" src="{{site.baseurl}}/public/{{person.picture}}"></a>{% endif %}
</td>
</tr>
{% endfor %}

</table>
